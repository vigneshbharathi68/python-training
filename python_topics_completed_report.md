# Topics covered in python


## Python basics (Click the link below in each Topics,that will take you to the file which I practised)
- [x] Operators 
    - [operator_overloading](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/08-09-2020/operator_overloading.py)
- [x] Variables
- [x] Iterators 
    - [iterator_func_modules](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/23-09-2020/iterator_func_modules.py)
    - [iterator Basic](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/08-09-2020/iterator.py)
- [x] Basic data types (List, Tuple, Dictionary and Sets)
    - [Arrays](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/11-09-2020/array.py)
    - [list advanced](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/16-09-2020/list_advanced.py)
        - [lists_tasks](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/16-09-2020/list_advanced.py)
    - [Dictionaries](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/16-09-2020/Dictionaries.py)
        - [Dictionary advanced](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/16-09-2020/Dictionaries.py)
        - [Dictionary completed tasks](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/24-09-2020/Dict_tasks.py)
    - [Tuples](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/17-09-2020/Tuple_advanced.py)
    - [Sets](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/17-09-2020/sets_advanced.py)
    - [Random practises](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/17-09-2020/random_practice.py)
- [x] Logical work flow (while, If-else)
    - [While loop](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/10-09-2020/while_loop.py)
    - [Exception handling](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/10-09-2020/exception_handling.py)
- [x] Functions
    - [functions_tasks](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/22-09-2020/functions_tasks.py)
    - [function_arguments](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/12-09-2020/function_arguments.py)
    - [function_arguments 2](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/11-09-2020/functions_arguments.py)
    - [Decorators](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/11-09-2020/decorator.py)
- [x] Classes
    - [Inheritance, Polymorphism](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/07-09-2020/inheritance,polymorphism.py)
    - [OOPs_class](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/05-09-2020/oops_basics.py)
    - [OOPs_Tasks](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/05-09-2020/oops_basics2.py)
- [ ] Modules / pip
- [ ] File handling
- [x] Other
    - [Filter,map,reduce](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/09-09-2020/filter,map,reduce.py)
    - [randoms](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/09-09-2020/random_practise.py)
    - [Random practice on 04-09-2020](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/04-09-2020/randow_practise.py)

## Intermediate

- [x] JSON
    - [Json basic](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/23-09-2020/Json_basics.py) 
- [x] Regular Expressions
    - [Regular expression basic](https://gitlab.com/vigneshbharathi68/python-training/-/blob/master/28-09-2020/regular_expressions.py)
- [ ] Virtual environment
 -[ ] Command-line arguments (argparse)
