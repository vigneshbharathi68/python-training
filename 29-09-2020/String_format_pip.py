#!/usr/bin/env python
# coding: utf-8

# In[2]:


#pip

import camelcase

c = camelcase.CamelCase()

txt = "Hello world"

print(c.hump(txt))
# Output
# Hello World


# In[4]:


#String format

txt = 49

c = "The price is {} rupees"

d = "The price is {:.2f} rupees"

print(c.format(txt)) # The price is 49 rupees
print(d.format(txt)) # The price is 49.00 rupees


# In[8]:


# Multiple Values

Quantity = 3
itemno = 345
price = 49

txt = "I want {} quantity of item number {} in {} rupees"

print(txt.format(Quantity, itemno, price))
# I want 3 quantity of item number 345 in 49 rupees

indexno_txt = "I want {1} quantity of item number {0} in {2:.1f} rupees"

print(indexno_txt.format(Quantity, itemno, price))
# I want 345 quantity of item number 3 in 49.0 rupees

