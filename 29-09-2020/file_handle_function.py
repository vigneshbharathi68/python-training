#!/usr/bin/env python
# coding: utf-8

# In[2]:


# Giong to create one html file with python

f = open("helloworld.html", 'wb')

# message = """<html>
# <head>Hello world</head>
# <body><p>Hello World</p></body>
# </html>"""

# f.write(message)
f.close()


# In[4]:


f = open("helloworld.html", 'r')

f.read()

f.close()


# In[7]:


f = open("helloworld.html", 'w')

f.write("<html><head>Hello world</head><body><p>Hello World</p></body></html>")

f.close()


# In[13]:


# defined one function view the contents of the files
def write_file(file_name):
    f = open(file_name, 'r')
    print(f.read())
    
# file_name = "helloworld"
write_file("helloworld.html")
# Output
# html><head>Hello world</head><body><p>Hello World</p></body></html>

